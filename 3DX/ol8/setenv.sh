export CATALINA_OPTS="${CATALINA_OPTS} -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=8009 -Dcom.sun.management.jmxremote.rmi.port=8009 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"
export MX_DEBUG_PORT=8004
export JAVA_OPTS="${JAVA_OPTS} -Duser.language=en -Duser.region=KR"
export JRE_HOME=/usr/lib/jvm/ibm-semeru-open-17-jdk