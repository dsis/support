# 개발환경 구축

Docker를 기반으로 하는 3DExperience Platform 구축에 필요한 제반 프로그램 설정에 관한 내용입니다. 모든 사용자는 관리는 OpenLDAP에 기반하므로, OpenLDAP은 반드시 설치 되어야 합니다. 필요한 부분만 설정하는 불필요한 부분은 Remark 처리하면 됩니다.

Linux에서 설치를 권장하며, Windows에서는 일부 제약 사항이 있습니다. [docker-compose.yml](./docker-compose.yml) 파일을 참고하기 바랍니다.

* Subversion은 https://httpd.mydomain.com/websvn 또는 https://httpd.mydomain.com/svn/3DX/ 와 같은 방식으로 접속하시면 됩니다.
* Jenkins 설치 시에 Internet 접속이 가능한 경우에는 Add On 설치 부분을 Remark 처리해도 됩니다.
* SSL/TLS 인증서는 반드시 새로 만들어서 사용하시기 바랍니다.
* Rainloop의 경우 주소 뒤에 반드시 "/"를 붙여야 합니다.

보다 상세한 내용은 Wiki를 참조하기 바랍니다.

```mermaid
graph TD
subgraph Mandatory
A(OpenLDAP Admin) --> B(OpneLDAP)
C(SubVersion) --> B
E(Jenkins) --> B
K --> E
end
D(Redmine) --> B
subgraph Redmine
D --> F(mariadb4redmine)
end
E --> C
E --> G(mailserver)
subgraph eMail
H(Rainloop) --> G
end
H --> B
I(GitLab) --> B
J(OpenFire) --> B
G --> B
K(Apache) --> A
K --> C
K --> D
K --> H
K --> I
L((User)) ==> K
L ==> M(bind)
L ==> G
L ==> J
```