#!/bin/sh
chown -R vmail:postdrop /mail

postconf -e "mydomain = ${DOMAINNAME}.${TLD}"
postconf -e "mynetworks = ${MAILNETWORK}"

sed -i -r '$ a\search_base = '"$LDAPSEARCHBASE"'' /etc/postfix/ldap-alias.cf
sed -i -r '$ a\bind_dn = cn=admin,dc='"$DOMAINNAME"',dc='"$TLD"'' /etc/postfix/ldap-alias.cf
sed -i -r '$ a\bind_pw = '"$LDAPADMINPASSWORD"'' /etc/postfix/ldap-alias.cf
sed -i -r '$ a\query_filter = '"$LDAPQUERYFILTER"'' /etc/postfix/ldap-alias.cf
sed -i -r '$ a\result_attribute = '"$LDAPRESULTATTR"'' /etc/postfix/ldap-alias.cf

sed -i -r '$ a\search_base = '"$LDAPSEARCHBASE"'' /etc/postfix/ldap-mailbox.cf
sed -i -r '$ a\bind_dn = cn=admin,dc='"$DOMAINNAME"',dc='"$TLD"'' /etc/postfix/ldap-mailbox.cf
sed -i -r '$ a\bind_pw = '"$LDAPADMINPASSWORD"'' /etc/postfix/ldap-mailbox.cf
sed -i -r '$ a\query_filter = '"$LDAPQUERYFILTER"'' /etc/postfix/ldap-mailbox.cf
sed -i -r '$ a\result_attribute = '"$LDAPRESULTATTR"'' /etc/postfix/ldap-mailbox.cf

sed -i -r '/#mail_location =/a\mail_location = maildir:/mail/'"$DOMAINNAME"'.'"$TLD"'/%n' /etc/dovecot/conf.d/10-mail.conf
sed -i -r '$ a\userdb {' /etc/dovecot/conf.d/auth-ldap.conf.ext; \
sed -i -r '$ a\  driver = static' /etc/dovecot/conf.d/auth-ldap.conf.ext; \
sed -i -r '$ a\  args = uid=vmail gid=postdrop home=/mail/'"$DOMAINNAME"'.'"$TLD"'/%n' /etc/dovecot/conf.d/auth-ldap.conf.ext; \
sed -i -r '$ a\}' /etc/dovecot/conf.d/auth-ldap.conf.ext
sed -i -r 's/base =/& dc='"$DOMAINNAME"',dc='"$TLD"'/' /etc/dovecot/dovecot-ldap.conf.ext
sed -i -r '/#auth_bind_userdn =/a\auth_bind_userdn = uid=%n,'"$LDAPSEARCHBASE"'' /etc/dovecot/dovecot-ldap.conf.ext
postfix start
dovecot -F