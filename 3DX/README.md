# Apache httpd 설치

```shell
yum install yum-utils rpm-build
yum groupinstall 'Development Tools'

yum config-manager --set-enabled ol8_codeready_builder
yum install libuuid-devel
wget https://archive.apache.org/dist/apr/apr-1.7.0.tar.bz2
rpmbuild -ts apr-1.7.0.tar.bz2
yum-builddep 
rpmbuild -tb apr-1.7.0.tar.bz2

wget --no-check-certificate https://dlcdn.apache.org/httpd/httpd-2.4.54.tar.bz2
rpmbuild -ts httpd-2.4.54.tar.bz2
rpmbuild -tb httpd-2.4.54.tar.bz2
```

```ksh
yum -y install epel-release
cd /etc/yum.repos.d && wget https://repo.codeit.guru/codeit.el`rpm -q --qf "%{VERSION}" $(rpm -q --whatprovides redhat-release)`.repo
yum list httpd
yum install httpd
```
