#!/bin/bash

openssl genrsa -out root.$1.key 2048
openssl req -new -sha256 -key root.$1.key -out root.$1.csr -subj $2/CN=root.$1
openssl x509 -req -sha256 -days $3 -in root.$1.csr -signkey root.$1.key -out root.$1.crt
openssl x509 -noout -text -in root.$1.crt

openssl genrsa -out server.$1.key 2048
openssl req -new -sha256 -key server.$1.key -out server.$1.csr -subj $2/CN=*.$1

echo authorityKeyIdentifier=keyid,issuer > $1.ext
echo basicConstraints=CA:FALSE >> $1.ext
echo keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment >> $1.ext
echo subjectAltName = @alt_names >> $1.ext
echo [alt_names] >> $1.ext
echo DNS.1 = *.$1 >> $1.ext
echo DNS.2 = localhost >> $1.ext
echo DNS.3 = *.localhost >> $1.ext

openssl x509 -req -sha256 -days $3 -in server.$1.csr -CA root.$1.crt -CAkey root.$1.key -CAcreateserial -out server.$1.crt -extfile $1.ext
openssl x509 -noout -text -in server.$1.crt