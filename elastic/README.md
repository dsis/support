# ELK Stack 기반 Platform Monitoring
## Elasticsearch 설치

docker-compose.yml 참조하여 설치한다.
Security module은 적용하지 않아 login 과정은 필요하지 않다.
접속은 kibana로 하면 된다.

![](images/chrome_pQn5oxITij.png)

Menu 하단의 `Add integrations`를 선택하고 필요한 부분을 설치한다.

![](images/chrome_pz2WJ7zaoA.png)
![](images/chrome_CuH7WSOqzO.png)

## Metricbeat 설정

Metricbeat의 설치는 각 Metricbeat module의 guide를 참조한다.

### Apache HTTP Server Metrics
![](images/chrome_5DqO1XzaV7.png)

이 모듈을 사용하기 위해서는 apache에 mod_status가 설치 되어 있어야 한다. Apache 설정에 다음을 추가한다.

```apache
<IfModule !status_module>
        LoadModule status_module modules/mod_status.so
</IfModule>

<Location "/server-status">
    SetHandler server-status
</Location>
```
![](images/nDN3hLz3YU.png)

### System Metrics
![](images/chrome_MsJmwTwvlX.png)

![](images/yfhJpdVFP3.png)
![](images/sBAEVPWsmx.png)

### Oracle Metrics
![](images/wUCcvuBJgw.png)

### Metricbeat Service 설치 오류

```powershell
PS D:\DS\Metricbeat> .\install-service-metricbeat.ps1
.\install-service-metricbeat.ps1 : File D:\DS\Metricbeat\install-service-metricbeat.ps1 cannot be loaded because
running scripts is disabled on this system. For more information, see about_Execution_Policies at
https:/go.microsoft.com/fwlink/?LinkID=135170.
At line:1 char:1
+ .\install-service-metricbeat.ps1
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : SecurityError: (:) [], PSSecurityException
    + FullyQualifiedErrorId : UnauthorizedAccess
```

위와 같은 오류 발생시 다음을 수행한다.

```powershell
Set-ExecutionPolicy -ExecutionPolicy unrestricted -scope LocalMachine
Get-ExecutionPolicy -list
```